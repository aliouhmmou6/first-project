<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(AuthController::class)->group(function(){
    Route::get('register','register')->name('register');
    Route::post('register','registerSave')->name('register.save');

    Route::get('login','login')->name('login');
    Route::post('login','loginAction')->name('login.action');

    Route::get('logout','logout')->middleware('auth')->name('logout');
});

Route::middleware('auth')->group(function(){
    Route::get('dashboard',function(){
        return view('dashboard');
    })->name('dashboard');

    // Route::resource('clients', ClientController::class);

    Route::controller(ClientController::class)->prefix('clients')->group(function(){
        Route::get('','index')->name('clients');
        Route::get('create','create')->name('clients.create');
        Route::post('store','store')->name('clients.store');
        // Route::get('show/{id}','show')->name('products.show');
        Route::get('edit/{id}','edit')->name('clients.edit');
        Route::put('edit/{id}','update')->name('clients.update');
        Route::delete('destroy/{id}','destroy')->name('clients.destroy');
    });
    // Route::controller(OrderController::class)->prefix('orders')->group(function(){
    //     Route::get('','index')->name('orders');
    //     // Route::get('create','create')->name('clients.create');
    //     Route::post('store','store')->name('orders.store');
    //     // Route::get('show/{id}','show')->name('products.show');
    //     // Route::get('edit/{id}','edit')->name('clients.edit');
    //     // Route::put('edit/{id}','update')->name('clients.update');
    //     Route::delete('destroy/{id}','destroy')->name('orders.destroy');
    // });
    // Route::post('orders','store')->name('orders.store');

    Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
    Route::post('/orders', [OrderController::class, 'store'])->name('orders.store');
    Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');
    Route::delete('/destroy/{id}', [OrderController::class, 'destroy'])->name('orders.destroy');
    Route::post('orders/{id}/update-status',[OrderController::class, 'updateStatus'])->name('orders.updateStatus');
    


    // Route::resource('orders', OrderController::class);

    Route::get('/checkout', [CartController::class, 'checkout'])->name('checkout');

    Route::get('/add-to-cart/{id}', [CartController::class, 'addToCart'])->name('add_to_cart');
    Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('remove_from_cart');
    Route::patch('update-cart', [CartController::class, 'update'])->name('update_cart');
    // Route::get('destroy', [CartController::class, 'remove'])->name('cart.destroy');
    Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
    // Route::post('/cart', [CartController::class, 'store'])->name('cart.store');
    // Route::post('/cart/change-qty', [CartController::class, 'changeQty']);
    // Route::delete('/cart/delete', [CartController::class, 'delete']);
    // Route::delete('/cart/empty', [CartController::class, 'empty']);

    Route::controller(ProductController::class)->prefix('products')->group(function(){
        Route::get('','index')->name('products');

        Route::get('create','create')->name('products.create');
        Route::post('store','store')->name('products.store');
        Route::get('show/{id}','show')->name('products.show');
        Route::get('edit/{id}','edit')->name('products.edit');
        Route::put('edit/{id}','update')->name('products.update');
        Route::delete('destroy/{id}','destroy')->name('products.destroy');
        Route::get('detail/{id}','detail')->name('products.detail');
    });

    // Route::get('/profile',[App\Http\Controllers\AuthController::class, 'profile'])->name('profile');
    Route::get('/profile', [ProfileController::class, 'profile'])->name('profile');
    Route::post('/profile/update', [ProfileController::class, 'update'])->name('profile.update');
    // Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    // Route::get('/get-filtered-data', [DashboardController::class, 'getFilteredData'])->name('getFilteredData');
    Route::get('/get-orders-by-month-data', [DashboardController::class, 'getOrdersByMonth'])->name('getOrdersByMonth');
    Route::get('/get-orders-by-client-data', [DashboardController::class, 'getOrdersByClientData'])->name('getOrdersByClientData');
    Route::get('/get-paid-unpaid-data', [DashboardController::class, 'getPaidUnpaidData'])->name('getPaidUnpaidData');
   
});
