<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('sku')->unique();

        });
    }



    // public function check()
    // {
    //     $products = 'products';
    //     $sku = 'sku';

    //     if (Schema::hasColumn($products, $sku)) {
    //         // Column already exists in the table
    //         Artisan::output()->info("sku '{$sku}' already exists in the '{$products}' table.");
    //     } else {
    //         // Add the new column to the table
    //         Schema::table($products, function (Blueprint $table) use ($sku) {
    //             $table->string($sku)->nullable();
    //         });

    //         // Display success message
    //         Artisan::output()->success("Column '{$sku}' added to the '{$products}' table.");
    //     }
    // }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
};
