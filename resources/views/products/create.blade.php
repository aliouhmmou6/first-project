@extends('layout.app')

@section('title', 'Create Product')

@section('contents')
    <h1 class="mb-0">Add Product</h1>

    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <hr />
    <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <strong>Sku:</strong>
                <input type="text" name="sku" class="form-control" placeholder="SKU">
            </div>
            <div class="col">
                <strong>Title:</strong>
                <input type="text" name="title" class="form-control" placeholder="Title">
            </div>
            <div class="col">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" placeholder="image">
            </div>

            <div class="col">
                <strong>Price:</strong>
                <input type="text" name="price" class="form-control" placeholder="00.00">
            </div>
            <div class="col">
                <strong>Quantity:</strong>
                <input type="text" name="quantity" class="form-control" placeholder="Quantity">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <strong>Product Code:</strong>
                <input type="text" name="product_code" class="form-control" placeholder="Product Code">
            </div>
            <div class="col">
                <strong>Description:</strong>
                <textarea class="form-control" name="description" placeholder="Descriptoin"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-secondary">Submit</button>
            </div>
        </div>
    </form>
@endsection
