@extends('layout.app')

@section('title', 'Create Client')

@section('contents')
    <h1 class="mb-0">Add New Client</h1>

    {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

    <hr />
    <form action="{{ route('clients.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <strong>First Name:</strong>
                <input type="text" name="first_name" class="form-control" placeholder="First Name">
            </div>
            <div class="col">
                <strong>Last Name:</strong>
                <input type="text" name="last_name" class="form-control" placeholder="Last Name">
            </div>
            <div class="col">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email">
            </div>

            <div class="col">
                <strong>Contact Number:</strong>
                <input type="text" name="phone" class="form-control" placeholder="Contact Number">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <strong>Address:</strong>
                <input type="text" name="address" class="form-control" placeholder="Address">
            </div>
            <div class="col">
                <strong>Logo Company:</strong>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="logo" id="logo">
                    <label class="custom-file-label" for="logo">Choose File</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-secondary">Submit</button>
            </div>
        </div>
    </form>
@endsection
