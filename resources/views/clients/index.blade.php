{{-- @extends('layouts.admin') --}}
@extends('layout.app')

@section('title', 'Client Management')
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Client Management</h1>
        <a href="{{route('clients.create')}}" class="btn btn-secondary">Add New Client</a>
    </div>
    <hr />
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{Session::get('success')}}
        </div>
    @endif
    <table class="table table-hover">
        <thead class="table-secondary">
            <tr>
                    <th>ID</th>
                    <th>Logo Company</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @if($clients->count() > 0)
                @foreach($clients as $client)
                    <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td><img src="/images/{{ $client->logo }}" width="100px"></td>
                        <td class="align-middle">{{ $client->first_name }} {{$client->last_name}}</td>
                        <td class="align-middle">{{ $client->email }}</td>
                        
                        <td class="align-middle">{{ $client->phone }}</td>
                        <td class="align-middle">{{ $client->address }}</td>
                        <td class="align-middle">{{ $client->created_at }}</td>
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                {{-- <a href="{{ route('clients.show', $client->id) }}" type="button" class="btn btn-secondary">Detail</a> --}}
                                <a href="{{ route('clients.edit', $client->id)}}" type="button" class="btn btn-warning">Edit</a>
                                <form action="{{ route('clients.destroy', $client->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger m-0">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">Client not found</td>
                </tr>
            @endif


                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>


@endsection
