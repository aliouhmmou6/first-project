@extends('layout.app')

@section('title', 'Orders List')
@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        <h1 class="mb-0">Orders List</h1>
        <a href="{{route('orders.create')}}" class="btn btn-secondary">Ajouter Order</a>
    </div>
    {{-- <form>
        <div class="row">
            <div class="col-md-5">
                <input type="date" name="start_date" class="form-control" value="{{request('start_date')}}" />
            </div>
            <div class="col-md-5">
                <input type="date" name="end_date" class="form-control" value="{{request('end_date')}}" />
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary" type="submit"><i class="fas fa-filter"></i> Filter</button>
            </div>
        </div>
    </form> --}}
    <hr />
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{Session::get('success')}}
        </div>
    @endif
    <table class="table table-hover">
        <thead class="table-secondary">
            <tr>
                <th>ID</th>
                <th>Client</th>
                <th>Payment Type</th>
                <th>Total Price</th>
                <th>Status</th>
                <th>Created At</th>
            </tr>
        </thead>
        <tbody>
            @if($orders->count() > 0)
            @foreach($orders as $order)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $order->client->first_name }} {{ $order->client->last_name }}</td>
                <td>{{ $order->payment_type }}</td>
                <td>{{ $order->total_price}}$</td>
                <td>
                    @if ($order->status === 'unpaid')
                    <span style="color: red;">{{ ucfirst($order->status) }}</span>
                    @elseif ($order->status === 'paid')
                    <span style="color: grey;">{{ ucfirst($order->status) }}</span>
                    @endif
                </td>
                
                <td>{{ $order->created_at }}</td>
                <td>
                    <div class="btn-group" role="group">
                        <form action="{{ route('orders.destroy', $order->id) }}" method="POST" type="button" class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                        @if ($order->status === 'unpaid')
                            <form action="{{ route('orders.updateStatus', $order->id) }}" method="POST" class="btn btn-secondary p-0 ml-2">
                                @csrf
                                <input type="hidden" name="status" value="paid">
                                <button type="submit" class="btn btn-secondary">Paid</button>
                            </form>
                        @endif
                    </div>
                </td>
                
            </tr>

            @endforeach
            @endif
        </tbody>
    </table>

    </div>
</div>
@endsection




