@extends('layout.app')

@section('title', 'Create Order')
@section('contents')

<h1>Create Order</h1>
{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<hr />

<form action="{{ route('orders.store') }}"  method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <div class="row mb-2">
                <div class="col">
                    <select class="form-control" name="client_id">
                        <option value="">Name Client</option>
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->first_name }} {{ $client->last_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <input type="date" name="start_date" class="form-control" value="" />
                </div>

            </div>
            <div class="form-group">
                <label for="payment_type">Payment Type</label>
                <select class="form-control" id="payment_type" name="payment_type">
                    <option value="cash">Cash</option>
                    <option value="check">Chequ</option>
                </select>
            </div>

            <div class="form-group" id="check_reference_container" style="display: none;">
                <label for="check_reference">Check Reference</label>
                <input type="text" class="form-control" id="check_reference" name="check_reference">
            </div>
        </div>

    </div>



    <button id="add-row" class="btn btn-secondary mb-3">Add Row</button>
    <table id="order-table" class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <!-- Rows for order items will be added here -->
            </tbody>
            <tfoot>
                <tr>
                    <td></td>

                    <td colspan="3"></td>
                    <td>Total: <span name="total_price" id="total-price">0</span></td>
                    <td></td>
                </tr>
            </tfoot>
    </table>

        <div class="row mt-4">
            <div class="col-md-6">
                <p>VAT (20%): <span id="vat">0</span></p>
                <p>Total Price (HT): <span id="total-price-ht">0</span></p>
            </div>
        </div>
        <div class="row">
            <div class="d-grid">
                <button type="submit" class="btn btn-secondary">Save Order</button>
            </div>
        </div>
</form>


@section('scripts')
<script>
    $(document).ready(function () {
        let total = 0;
        var rowCounter = 0;

        // Add a new row when the "Add Row" button is clicked
        $('#add-row').click(function () {
            const cont = ++rowCounter
            const newRow = `
                <tr>
                    <td>${cont}</td>
                    <td>
                        <select class='form-control product' name='product[]' id='product${cont}' data-id="${cont}" required>
                            <option value="">Select Product</option>
                            @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ $product->title }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="number" name="quantity[]" class="form-control" required>
                    </td>
                    <td>
                        <input value="" type="number" name="price[]" id='price${cont}' class="form-control" required>
                    </td>
                    <td>
                        <span name="total_price" class="total-price">0</span>
                    </td>
                    <td>
                        <button class="btn btn-danger delete-row">Delete</button>
                    </td>

                </tr>
            `;

            $('tbody').append(newRow);
        });

        // Update total and individual row total when values change
        $('tbody').on('input', 'input[name="quantity[]"], input[name="price[]"]', function () {
            const row = $(this).closest('tr');
            const quantity = parseFloat(row.find('input[name="quantity[]"]').val());
            const price = parseFloat(row.find('input[name="price[]"]').val());

            const rowTotal = quantity * price;
            row.find('.total-price').text(rowTotal.toFixed(2));

            updateTotal();
        });

        // Delete a row
        $('tbody').on('click', '.delete-row', function () {
            $(this).closest('tr').remove();
            updateTotal();
            updateRowNumbers();
        });
        function updateRowNumbers() {
            rowCounter = 0;
            $('tbody tr').each(function () {
                $(this).find('td:first').text(++rowCounter);
            });
        }

        // Update total price
        function updateTotal() {
            total = 0;
            $('.total-price').each(function () {
                total += parseFloat($(this).text());
            });
            $('#total-price').text(total.toFixed(2));
        }

        // Submit the form when "Submit Order" is clicked
        $('#submit-order').click(function () {
            $('#order-form').submit();
        });
    });


    $(document).ready(function () {
        $('#payment_type').change(function () {
            if ($(this).val() === 'check') {
                $('#check_reference_container').show();
            } else {
                $('#check_reference_container').hide();
            }
        });
    });


    $(document).ready(function () {
        let total = 0;

        // Add a new row when the "Add Row" button is clicked
        $('#add-row').click(function () {
            // ... Existing code ...

            // Recalculate totals
            updateTotal();
            calculateVAT();
        });

        // Update total and individual row total when values change
        $('tbody').on('input', 'input[name="quantity[]"], input[name="price[]"]', function () {
            // ... Existing code ...

            // Recalculate totals
            updateTotal();
            calculateVAT();
        });

        // Delete a row
        $('tbody').on('click', '.delete-row', function () {
            // ... Existing code ...

            // Recalculate totals
            updateTotal();
            calculateVAT();
        });

        // Update total price
        function updateTotal() {
            total = 0;
            $('.total-price').each(function () {
                total += parseFloat($(this).text());
            });
            $('#total-price').text(total.toFixed(2));
        }

        // Calculate VAT and Total Price (including VAT)
        function calculateVAT() {
            const vatRate = 0.20; // 20%
            const totalPrice = parseFloat($('#total-price').text());

            const vatAmount = totalPrice * vatRate;
            const totalPriceIncludingVAT = totalPrice + vatAmount;

            $('#vat').text(vatAmount.toFixed(2));
            $('#total-price-ht').text(totalPriceIncludingVAT.toFixed(2));
        }


    });

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>

$(document).on('change', '.product', function(){
                var row = $(this).data('id');
                var productrow = '#product'+row ;
                var id = $(productrow).val();
                var pricerow = '#price'+row;
                $.ajax({
                url: '/products/detail/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    // var priceValue = response;
                    $(pricerow).val(response);
                    // console.log('Price Value:', priceValue);
                    console.log('Price Element:', $(pricerow));
                    console.log(response);

                }
                });

            });
    // $(function () {
    //     // province Change
    //     $('.product2').change(function(){
    //         // Department id
    //         alert('pp');
    //         var row = $(this).data('id');
    //         var productrow = '#product'+row ;
    //         var id = $(productrow).val();
    //         var pricerow = '#price'+row;
    //         // AJAX request
    //         $.ajax({
    //         url: 'product-price/'+id,
    //         type: 'get',
    //         dataType: 'json',
    //         success: function(response){


    //             $(pricerow).val(response['data']);

    //         }
    //         });
    //     });
    // });
</script>
@endsection

@endsection
