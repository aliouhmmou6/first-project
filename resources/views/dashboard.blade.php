@extends('layout.app')

@section('title', 'Dashboard - Laravel Projet App')

@section('contents')
<div class="row">
    <div class="col-md-6">  
        <div class="form-row mb-2">
            <div class="col-md-6">
                <input type="date" id="date" class="form-control form-control-sm" placeholder="Start Date">
            </div>
            
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <button id="filter_paid_unpaid" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>
        <div class="mt-2">
            <canvas id="ordersByMonthBarChart"></canvas>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-row mb-2">
            <div class="col-md-6">
                <input type="date" id="date" class="form-control form-control-sm" placeholder="Start Date">
            </div>
            
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <button id="filter_paid_unpaid" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>
        <div class="mt-2">
            <canvas id="clientOrdersBarChart"></canvas>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="form-row mb-2">
            <div class="col-md-6">
                <input type="date" id="date" class="form-control form-control-sm" placeholder="Start Date" pattern="\d{4}-\d{2}-\d{2}">
            </div>
            
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <button id="filter_paid_unpaid" class="btn btn-primary btn-sm">Filter</button>
            </div>
        </div>
        <div class="mt-2">
            <canvas id="paidUnpaidChart"></canvas>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
    $(document).ready(function () {
        $.ajax({
            url: '/get-orders-by-client-data',
            success: function(response) {
                // console.log(response);
                var data = response.data;
                var labels = data.map(item => item.client);
                var paidOrders = data.map(item => item.paid);
                var unpaidOrders = data.map(item => item.unpaid);

                var ordersByClientBarCanvas = document.getElementById('clientOrdersBarChart').getContext('2d');
                var ordersByClientBarChart = new Chart(ordersByClientBarCanvas, {
                    type: 'bar',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: 'Paid Orders',
                            data: paidOrders,
                            backgroundColor: '#27ae60',
                            borderWidth: 1
                        },
                        {
                            label: 'Unpaid Orders',
                            data: unpaidOrders,
                            backgroundColor: '#e74c3c',
                            borderWidth: 1
                        }]
                    }
           
                });
            }
});
        // Fetch order by month data and create bar chart
        $.ajax({
            url: '/get-orders-by-month-data',
            success: function(data) {
                var ordersByMonthBarCanvas = document.getElementById('ordersByMonthBarChart').getContext('2d');
                var ordersByMonthBarChart = new Chart(ordersByMonthBarCanvas, {
                    type: 'bar',
                    data: {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        datasets: [{
                            label: 'Orders by Month',
                            data: data.orders_by_month,
                            backgroundColor: '#3498db',
                            borderWidth: 1
                        }]
                    },
                    
                });
            }
        });
        



        // Fetch paid/unpaid data and create pie chart
        $.ajax({
            url: '/get-paid-unpaid-data',
                       
            success: function(data) {
                var paidUnpaidCanvas = document.getElementById('paidUnpaidChart').getContext('2d');
                var paidUnpaidChart = new Chart(paidUnpaidCanvas, {
                    type: 'pie',
                    data: {
                        labels: ['Paid', 'Unpaid'],
                        datasets: [{
                            data: [data.paid, data.unpaid],
                            backgroundColor: ['#27ae60', '#e74c3c'],
                        }],
                    },
                });
            }
        });
    

    
        
      
    });
</script>
@endsection




