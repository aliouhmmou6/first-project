<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'logo'
    ];

    public function getLogoUrl()
    {
        return Storage::url($this->logo);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'client_id');
    }
}
