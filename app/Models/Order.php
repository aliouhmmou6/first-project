<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'payment_type',
        'check_reference',
        'status',
        'total_price'
        // 'start_date',
    ];


    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProducts::class, 'order_id');
    }



}



