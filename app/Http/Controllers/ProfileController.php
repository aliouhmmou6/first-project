<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('profile');
    }

    public function update(Request $request) {
        $user = auth()->user();

    // Example:
    DB::table('users')
        ->where('id', $user->id)
        ->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
        ]);

    // Redirect back to the profile page or any other appropriate action
    return redirect()->back()->with('success', 'Profile updated successfully');
    }
}
