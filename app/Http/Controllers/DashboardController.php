<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Client;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    // public function index()
    // {
    //     $ordersCount = Order::count();
    //     $clientsCount = Client::count();
    //     $productsCount = Product::count();

    //     return view('dashboard', compact('ordersCount', 'clientsCount', 'productsCount'));
    // }


    public function getOrdersByMonth(Request $request)
    {
        // $start_date = $request->input('start_date');
        // $end_date = $request->input('end_date');

         
        $orders_by_month = Order::selectRaw('MONTH(created_at) as month, COUNT(*) as count')
            // ->whereBetween('created_at', [$start_date, $end_date])
            ->groupBy('month')
            ->orderBy('month')
            ->pluck('count', 'month')
            ->toArray();

        // Fill in missing months with zero count
        for ($month = 1; $month <= 12; $month++) {
            if (!isset($orders_by_month[$month])) {
                $orders_by_month[$month] = 0;
            }
        }

        ksort($orders_by_month);

        return response()->json(['orders_by_month' => array_values($orders_by_month)]);
    }

    // public function getOrdersByClientData(Request $request)
    // {
    //     $clients = Client::all();

    //     // Prepare data for the chart
    //     $clientNames = $clients->pluck('name');
    //     $paidOrders = $clients->pluck('paid_orders_count');
    //     $unpaidOrders = $clients->pluck('unpaid_orders_count');
    //     return view('charts.client_orders_chart', compact('clientNames', 'paidOrders', 'unpaidOrders'));

    // }

    //     public function clientOrdersChart()
    // {
    //     $clients = Client::all();
    //     $data = [];

    //     foreach ($clients as $client) {
    //         $data['labels'][] = $client->name;
    //         $data['paid'][] = $client->paid_orders_count;
    //         $data['unpaid'][] = $client->unpaid_orders_count;
    //     }

    //     return view('charts.client_orders_chart', compact('data'));
    // }



    public function getOrdersByClientData()
    {
        $clients = Client::with('orders')->get();
        $data = [];

        foreach ($clients as $client) {
            $full_name = $client->first_name . ' ' . $client->last_name;
            $paidOrders = $client->orders->where('status', 'paid')->count();
            $unpaidOrders = $client->orders->where('status',
                'unpaid'
            )->count();

            $data[] = [
                'client' => $full_name,
                'paid' => $paidOrders,
                'unpaid' => $unpaidOrders
            ];
        }

        return response()->json(['data' => $data]);
    }



    public function getPaidUnpaidData(Request $request)
    {
   
        $paidCount = Order::where('status', 'paid')->count();
        $unpaidCount = Order::where('status', 'unpaid')->count();

        $data = [
            'paid' => $paidCount,
            'unpaid' => $unpaidCount,
        ];
    

        return response()->json($data);
    }

  


//     public function getPaidUnpaidData(Request $request)
// {
//     $selectedDate = $request->input('date');
//     $startDate = Carbon::parse($selectedDate)->startOfDay();
//     $endDate = Carbon::parse($selectedDate)->endOfDay();

//     // Query your database to filter data based on the date range
//     $paidCount = Order::where('status', 'paid')
//         ->whereBetween('created_at', [$startDate, $endDate])
//         ->count();

//     $unpaidCount = Order::where('status', 'unpaid')
//         ->whereBetween('created_at', [$startDate, $endDate])
//         ->count();

//     $data = [
//         'paid' => $paidCount,
//         'unpaid' => $unpaidCount,
//     ];

//     return response()->json($data);
// }


   


   

}
