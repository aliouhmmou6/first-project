<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientStoreRequest;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {


        // $clients = Client::latest()->paginate(5);

        // return view('clients.index',compact('clients'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);

        if (request()->wantsJson()) {
            return response(
                Client::all()
            );
        }
        $clients = Client::latest()->paginate(10);
        return view('clients.index')->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        // $request->validate([
        //     'first_name' => 'required',
        //     'last_name' => 'required',
        //     'email' => 'required',
        //     'phone' => 'required',
        //     'address' => 'required',
        //     'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        //     // 'user_id' => 'required'

        // ]);

        $input = $request->all();

        if ($logo = $request->file('logo')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $profileImage);
            // dd($profileImage);
            // $data = array();
            // $data['first_name'] = $request->first_name;
            // $data['last_name'] = $request->last_name;
            // $data['email'] = $request->email;
            // $data['phone'] = $request->phone;
            // $data['address'] = $request->address;
            // $data['logo'] = $request->logo;

            $input['logo'] = $profileImage;
        }


        Client::create($input);

        return redirect()->route('clients')->with('success', 'Client added successfully');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $client = Client::findOrFail($id);

        return view('clients.edit', compact('client'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Client $client)
    {
        // $request->validate([
        //     'first_name' => 'required',
        //     'last_name' => 'required',
        //     'email' => 'required',
        //     'phone' => 'required',
        //     'address' => 'required',
        //     'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        //     // 'user_id' => 'required'

        // ]);

        $input = $request->all();

        if ($logo = $request->file('logo')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $logo->getClientOriginalExtension();
            $logo->move($destinationPath, $profileImage);
            $input['logo'] = "$profileImage";
        }else{
            unset($input['logo']);
        }

        $client->update($input);

        return redirect()->route('clients')->with('success', 'client updated successfully');



    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {

        $client = Client::findOrFail($id);

        $client->delete();

        return redirect()->route('clients')->with('success', 'client deleted successfully');
    //     if ($client->logo) {
    //         Storage::delete($client->logo);
    //     }

    //     $client->delete();

    //    return response()->json([
    //        'success' => true
    //    ]);
    }
}
