<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Contracts\Session\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $product = Product::orderBy('created_at', 'DESC')->get();

        return view('products.index', compact('product'));
    }

    // public function addToCart($id)
    // {
    //     $product = Product::findOrFail($id);
    //     $cart = session()->get('cart',[]);
    //     if(isset($cart[$id])){
    //         $cart[$id]['quantity']++;
    //     }else{
    //         $cart[$id]=[
    //             'sku'=>$product->sku,
    //             'title'=>$product->title,
    //             'image'=>$product->image,
    //             'price'=>$product->price,
    //             'quantity'=>1,
    //             'product_code'=>$product->product_code,
    //             'description'=>$product->description
    //         ];
    //     }
    //     Session()->put('cart',$cart);
    //     return redirect()->back()->with('success','Product add to ssuccess');
    // }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'sku' => 'required',
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required',
            'product_code' => 'required',
            'description' => 'required'

        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);

            $input['image'] = $profileImage;
        }


        Product::create($input);

        return redirect()->route('products')->with('success', 'Product added successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = Product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {

        $request->validate([
            'sku' => 'required',
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required',
            'product_code' => 'required',
            'description' => 'required'
        ]);

        $input = $request->all();

        if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }else{
            unset($input['image']);
        }

        $product->update($input);

        return redirect()->route('products')->with('success', 'product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return redirect()->route('products')->with('success', 'product deleted successfully');
    }

    


    // public function checkSku($sku)
    // {
    //     $product = Product::where('sku', $sku)->first();

    //     if ($product) {
    //         // Product with the given SKU exists in the database
    //         return response()->json(['message' => 'SKU exists', 'sku' => $product->sku]);
    //     } else {
    //         // Product with the given SKU does not exist in the database
    //         return response()->json(['message' => 'SKU does not exist']);
    //     }
    // }

    // public function checkSku1(Request $request)
    // {
    //     $request->validate([
    //         'sku' => 'required|string',
    //     ]);

    //     $sku = $request->input('sku');
    //     $product = Product::where('sku', $sku)->first();

    //     if ($product) {
    //         $message = 'SKU exists';
    //         $foundSku = $product->sku;
    //     } else {
    //         $message = 'SKU does not exist';
    //         $foundSku = null;
    //     }

    //     return view('check_sku', compact('message', 'foundSku'));
    // }

    public function detail(Request $request,$id)
    {
        $product = Product::where('id',$id)->first();
        $product = $product->price;

        return response()->json($product);
    }
}
