<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Models\Client;
use App\Models\Order;
use App\Models\OrderProducts;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orders = Order::orderBy('created_at', 'DESC')->get();

        return view('orders.index', compact('orders'));

        // if (request()->wantsJson()) {
        //     return response(
        //         Order::all()
        //     );
        // }
        // $orders = Order::latest()->paginate(10);
        // return view('orders.index')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     */
    // public function create()
    // {
    //     $clients = Client::all(); // Fetch the list of clients
    // return view('orders.create', compact('clients'));
    // }

    public function create()
    {
        $products = Product::all();
        $clients = Client::all();

        return view('orders.xxx', compact('products', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     */
    // public function store(Request $request)
    // {
    //     $input = $request->all();
    //     Order::create($input);

    //     return redirect()->route('orders.index')->with('success', 'orders added successfully');
    // }


    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required',
            'payment_type' => 'required',
            'product' => 'required|array',
            'quantity' => 'required|array',
            'price' => 'required|array',
        ]);

        $order = Order::create([
            'client_id' => $request->input('client_id'),
            'payment_type' => $request->input('payment_type'),
            'check_reference' => $request->input('check_reference'),
            'status' => 'unpaid',
        ]);

        $totalPrice = 0;

        foreach ($request->input('product') as $key => $productId) {
            $quantity = $request->input('quantity')[$key];
            $price = $request->input('price')[$key];

            $totalPrice += $quantity * $price;

            OrderProducts::create([
                'order_id' => $order->id,
                'product_id' => $productId,
                'quantity' => $quantity,
                'total_price' => $quantity * $price,
            ]);
        }

        $order->update(['total_price' => $totalPrice]);

        return redirect()->route('orders.index')->with('success', 'Order added successfully');
    }

    
    public function updateStatus(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        // Toggle the status
        $newStatus = ($order->status === 'unpaid') ? 'paid' : 'unpaid';
        $order->update(['status' => $newStatus]);

        return redirect()->route('orders.index')->with('success', 'Order status updated successfully');
    }




    // public function store(Request $request)
    // {
    //     // Validate the request data
    //     $request->validate([
    //         'client_id' => 'required',
    //         // 'start_date' => 'required|date',
    //         'payment_type' => 'required',
    //         'product' => 'required|array',
    //         'quantity' => 'required|array',
    //         'price' => 'required|array',
    //     ]);



    //     Order::create($request->all());

    //     return redirect()->route('orders.index')->with('success', 'Orders added successfully');


    // }

    /**
     * Display the specified resource.
     */


    public function show($id)
    {



        // $order->load('orderItems.product');

        // return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    public function destroy(string $id)
    {
        $orders = Order::findOrFail($id);

        $orders->delete();

        return redirect()->route('orders.index')->with('success', 'orders deleted successfully');
    }


    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Order $order)
    // {
    //     $order->delete();

    //     return redirect()->route('orders.index')
    //         ->with('success', 'Order deleted successfully');
    // }
}

















// public function index(Request $request)
//     {
//         $orders = new Order();
//         if($request->start_date) {
//             $orders = $orders->where('created_at', '>=', $request->start_date);
//         }
//         if($request->end_date) {
//             $orders = $orders->where('created_at', '<=', $request->end_date . ' 23:59:59');
//         }
//         $orders = $orders->with(['products', 'payments', 'client'])->latest()->paginate(10);

//         $total = $orders->map(function($i) {
//             return $i->total();
//         })->sum();
//         $receivedAmount = $orders->map(function($i) {
//             return $i->receivedAmount();
//         })->sum();

//         return view('orders.index', compact('orders', 'total', 'receivedAmount'));

//         // $orders = Order::latest()->paginate(10);

//         // return view('orders.index', compact('orders'));
//     }



// public function store(OrderStoreRequest $request)
//     {
//         $order = Order::create([
//             'client_id' => $request->client_id,
//             'user_id' => $request->user()->id,
//         ]);

//         $cart = $request->user()->cart()->get();
//         foreach ($cart as $product) {
//             $order->items()->create([
//                 'price' => $product->price * $product->pivot->quantity,
//                 'quantity' => $product->pivot->quantity,
//                 'product_id' => $product->id,
//             ]);
//             $product->quantity = $product->quantity - $product->pivot->quantity;
//             $product->save();
//         }
//         $request->user()->cart()->detach();
//         $order->payments()->create([
//             'amount' => $request->amount,
//             'user_id' => $request->user()->id,
//         ]);
//         return 'success';
//     }
